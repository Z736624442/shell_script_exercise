#!/bin/bash
j=`whereis java`
java=$(echo ${j} | grep "jdk")
if [[ "$java" != "" ]]
then
echo "java was installed!"
else
echo "java not installed!"
cd ~/ 
mkdir /usr/local/java
cd /usr/local/java
wget https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html
tar -zvxf jdk-8u291-linux-x64.tar.gz 
echo 'JAVA_HOME=/usr/local/java/jdk1.8.0_151' >> /etc/profile 
echo 'JRE_HOME=/usr/local/java/jdk1.8.0_151/jre' >> /etc/profile
echo 'PATH=PATH :JAVA_HOME/bin:$JRE_HOME/bin' >> /etc/profile 
echo 'export JAVA_HOME JRE_HOME CLASS_PATH PATH' >> /etc/profile 
source /etc/profile
fi
cd jdk* && jdkname=`pwd | awk -F '/' '{print $NF}'`
echo "获取jdk版本: ${jdkname}"

## 测试

编写一个java的HelloWorld.java程序，看看jdk是否正常工作了。

```java
public class HelloWorld {
    public static void main(String[] args){
        System.out.println("Hello World!");
    }
}
```

```bash
javac HelloWorld.java

java HelloWorld
```
