#!/bin/bash

echo -n "Input Max Int Lines: "
read Max

for i in $(seq 1 $Max)
do
        for j in $(seq 1 $i)
        do
        f=$(($i-1))
        g=$(($j-1))
        if [ $j = 1 ]
        then
                declare SUM_${i}_$j=1
        else
                declare  A=$[SUM_${f}_$j]
                declare  B=$[SUM_${f}_$g]
                declare  SUM_${i}_$j=`expr $A + $B`
        fi
        echo -n $[SUM_${i}_$j]
        echo -en "  "
        done
echo "  "
done