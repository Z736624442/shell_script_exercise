#!/bin/sh

index=0
char=[ "|" "/" "-" "\\" ]
i=0
ret='#'
while [ $i -le 100 ]
do
        printf "\033[2J\033[40;32m[%-100s] [%2d%%] [%c]\033[0m\r" "$ret" "$i" "${char[$index]}"
        ret='#'$ret
        let i++
        let index++
        let index%=4
        printf "\n"

        sleep 1
done